provider "aws" {
    region = "ap-south-1"
}

variable "cidr_blocks" {
    description = "cidr blocks and name tag for VPC and subnet"
    type = list(object({
        cidr_block = string
        name = string
    }))
 }

variable "awail_zone" {}

resource "aws_vpc" "development-vpc" {
    cidr_block = var.cidr_blocks[0].cidr_block
    tags = {
        Name: var.cidr_blocks[0].name
        env_dev: "dev"
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = var.cidr_blocks[1].cidr_block
    availability_zone = var.awail_zone
     tags = {
        Name: var.cidr_blocks[1].name
    }
}

